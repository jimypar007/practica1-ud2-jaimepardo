CREATE DATABASE recambiosJaimePardo;
USE recambiosJaimePardo;

CREATE TABLE coches(
id int auto_increment primary key,
matricula VARCHAR(20) not null UNIQUE,
marca VARCHAR(40) not null,
potencia INTEGER,
fecha_publicacion TIMESTAMP
);
